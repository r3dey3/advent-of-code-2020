// hello.go

package main

import (
	//"flag"
	"fmt"
	//"io/ioutil"
	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
	"sort"
	//"strconv"
	//"strings"
	//"time"
)

func main() {
	max := 0

	//aoc.Lines = []string{"FBFBBFFRLR", "BFFFBBFRRR"}

	var ids []int
	for _, l := range aoc.Lines {
		row_min := 0
		row_max := 127
		col_min := 0
		col_max := 7
		for _, c := range l {
			switch c {
			case 'F':
				row_max = row_min + (row_max-row_min)/2
			case 'B':
				row_min = row_min + (row_max-row_min)/2 + 1
			case 'L':
				col_max = col_min + (col_max-col_min)/2
			case 'R':
				col_min = col_min + (col_max-col_min)/2 + 1
			}
		}
		id := row_min*8 + col_min
		if id > max {
			max = id
		}
		ids = append(ids, id)
	}
	fmt.Println(max)
	sort.Ints(ids)
	fmt.Println(ids)
	last := 0
	for _, i := range ids {
		if i-last > 1 {
			fmt.Println(i - 1)
		}
		last = i
	}
}
