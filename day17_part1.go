// hello.go

package main

import (
	"fmt"

	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
)

var field map[int]map[int]map[int]bool

func set(x, y, z int, field map[int]map[int]map[int]bool) {
	yx, ok := field[z]
	if !ok {
		field[z] = make(map[int]map[int]bool, 0)
		field[z][y] = make(map[int]bool, 0)
		field[z][y][x] = true
		return
	}
	x_row, ok := yx[y]
	if !ok {
		field[z][y] = make(map[int]bool, 0)
		field[z][y][x] = true
		return
	}
	x_row[x] = true
}
func check(x, y, z int, field map[int]map[int]map[int]bool) bool {
	_, ok := field[z][y][x]
	return ok
}
func clear(x, y, z int) {
	//_, ok := field[z][y][x]
	//return ok
}
func dup(in map[int]map[int]map[int]bool) map[int]map[int]map[int]bool {
	ret := make(map[int]map[int]map[int]bool, 0)

	for k_z, v_z := range in {
		ret[k_z] = make(map[int]map[int]bool, len(v_z))
		for k_y, v_y := range v_z {
			ret[k_z][k_y] = make(map[int]bool, len(v_y))
			for k_x, v_x := range v_y {
				if v_x {
					ret[k_z][k_y][k_x] = true
				}
			}
		}
	}
	return ret
}

func count_near(x_in, y_in, z_in int, field map[int]map[int]map[int]bool) int {
	count := 0
	for x := -1; x <= 1; x++ {
		for y := -1; y <= 1; y++ {
			for z := -1; z <= 1; z++ {
				if x == 0 && y == 0 && z == 0 {
					continue
				}
				if check(x_in+x, y_in+y, z_in+z, field) {
					count++
				}
			}
		}
	}
	return count
}
func main() {
	fmt.Println(len(aoc.Lines))
	field = make(map[int]map[int]map[int]bool, 0)
	field[0] = make(map[int]map[int]bool, len(aoc.Lines))

	for y, v_y := range aoc.Lines {
		field[0][y] = make(map[int]bool, len(v_y))
		for x, v_x := range v_y {
			if v_x == '#' {
				field[0][y][x] = true
			}
		}
	}

	min_z := 0
	max_z := 1
	min_x := 0
	max_x := len(aoc.Lines)
	min_y := 0
	max_y := len(aoc.Lines[0])

	fmt.Println(field)
	for z := min_z; z <= max_z; z++ {
		fmt.Println("Z = ", z)
		for y := min_y; y <= max_y; y++ {
			for x := min_x; x <= max_x; x++ {
				if check(x, y, z, field) {
					fmt.Printf("#")
				} else {
					fmt.Printf(".")
				}
			}
			fmt.Println("")
		}
		fmt.Println("")
	}

	for i := 0; i < 6; i++ {
		min_z--
		max_z++
		min_x--
		max_x++
		min_y--
		max_y++
		new_f := make(map[int]map[int]map[int]bool, 0)
		for z := min_z; z <= max_z; z++ {
			fmt.Println("Z = ", z)
			for y := min_y; y <= max_y; y++ {
				for x := min_x; x <= max_x; x++ {
					is_set := false
					near := count_near(x, y, z, field)
					if check(x, y, z, field) {
						if near == 2 || near == 3 {
							set(x, y, z, new_f)
							is_set = true
						}
					} else {
						if near == 3 {
							set(x, y, z, new_f)
							is_set = false
						}
					}
					if is_set {
						fmt.Printf("#")
					} else {
						fmt.Printf(".")
					}
				}
				fmt.Println("")
			}
			fmt.Println("")
		}
		field = new_f
	}

	count := 0
	for _, v_z := range field {
		for _, v_y := range v_z {
			for _, v_x := range v_y {
				if v_x {
					count++
				}
			}
		}
	}
	fmt.Println(count)

}
