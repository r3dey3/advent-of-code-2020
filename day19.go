// hello.go

package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
)

type Rule struct {
	orig string
	re   string
	gen  bool
}

var rules map[int]Rule

func get_re(idx int) string {
	v, ok := rules[idx]
	if !ok {
		panic(idx)
	}
	if v.gen {
		return v.re
	}
	if v.orig[0] == '"' {
		v.re = v.orig[1:2]
		v.gen = true
		rules[idx] = v
		return v.re
	}

	if idx == 11 {
		l := get_re(42)
		r := get_re(31)
		rule := "(" + l + r + ")"
		c_r := ""
		for i := 0; i < 20; i++ {
			c_r = l + c_r + r
			rule += "|(" + c_r + ")"
		}

		v.re = "(" + rule + ")"
		v.gen = true
		rules[idx] = v
		return v.re
	}
	rule := ""
	for _, group := range strings.Split(v.orig, " | ") {
		if rule != "" {
			rule += "|"
		}
		rule += "("
		for _, sub_s := range strings.Split(group, " ") {
			sub_n, _ := strconv.Atoi(sub_s)
			rule += get_re(sub_n)
		}
		rule += ")"
	}
	v.re = "(" + rule + ")"
	v.gen = true
	if idx == 8 {
		v.re += "+"
	}
	rules[idx] = v
	return v.re

}
func main() {

	rules = make(map[int]Rule, 0)
	for _, l := range aoc.Lines {
		if l == "" {
			break
		}
		v := strings.Split(l, ": ")
		idx, _ := strconv.Atoi(v[0])
		rules[idx] = Rule{v[1], "", false}
	}

	fmt.Println(len(aoc.Lines))
	fmt.Println(get_re(0))

	tests := false
	reg, _ := regexp.Compile("^" + get_re(0) + "$")
	count := 0
	for _, l := range aoc.Lines {

		if l == "" {
			tests = true
			continue
		}
		if !tests {
			continue
		}
		if reg.MatchString(l) {
			count++
		}
	}
	fmt.Println(count)
}
