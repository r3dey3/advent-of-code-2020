// hello.go

package main

import (
	//"flag"
	"fmt"
	//"io/ioutil"
	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
	//"strings"
	//"strconv"
	"time"
)

func combo_work(size int, pos int, alpha []int, start int, c chan []int, val [10]int) {
	if pos == size {
		c <- val[0:size]
		return
	}
	for idx := start; idx < len(alpha); idx++ {
		if idx > len(alpha)-(size-pos) {
			continue
		}
		val[pos] = alpha[idx]
		combo_work(size, pos+1, alpha, idx+1, c, val)
	}
}

func combo(size int, alpha []int) chan []int {
	c := make(chan []int)
	go func() {
		defer close(c)
		var v [10]int
		combo_work(size, 0, alpha, 0, c, v)
	}()

	return c
}

func combo_cb_work(size int, pos int, alpha []int, start int, val []int, cb func([]int) bool) bool {
	if pos == size {
		return cb(val)
	}
	for idx := start; idx < len(alpha); idx++ {
		if idx > len(alpha)-(size-pos) {
			continue
		}
		val[pos] = alpha[idx]
		if combo_cb_work(size, pos+1, alpha, idx+1, val, cb) {
			return true
		}
	}
	return false
}
func combo_cb(size int, alpha []int, cb func([]int) bool) {
	v := make([]int, size)
	combo_cb_work(size, 0, alpha, 0, v, cb)
}

func main() {
	if aoc.FileRead {
		fmt.Println("Successfully Read File")
	}
	c := combo(3, []int{1, 2, 3, 4, 5})
	for v := range c {
		fmt.Println(v)
	}
	combo_cb(3, []int{1, 2, 3, 4, 5},
		func(v []int) bool {
			fmt.Println(v)
			return false
		})

	start := time.Now()
	c = combo(2, aoc.Nums)
	for v := range c {
		if v[0]+v[1] == 2020 {
			fmt.Println(v[0] * v[1])
		}
	}
	fmt.Println("combo 2Took", time.Since(start))

	start = time.Now()
	c = combo(3, aoc.Nums)
	for v := range c {
		if v[0]+v[1]+v[2] == 2020 {
			fmt.Println(v[0] * v[1] * v[2])
		}
	}
	fmt.Println("combo 3 Took", time.Since(start))

	start = time.Now()
	combo_cb(3, aoc.Nums,
		func(v []int) bool {
			if v[0]+v[1]+v[2] == 2020 {
				fmt.Println(v[0] * v[1] * v[2])
			}
			return false
		})
	fmt.Println("callback 3 Took", time.Since(start))

	start = time.Now()
	for idx, n := range aoc.Nums {
		for idx2 := idx + 1; idx2 < len(aoc.Nums); idx2++ {
			if n+aoc.Nums[idx2] == 2020 {
				fmt.Println(n * aoc.Nums[idx2])
			}
		}
	}
	fmt.Println("brute 2 Took", time.Since(start))
	start = time.Now()
	for idx, n := range aoc.Nums {
		for idx2 := idx + 1; idx2 < len(aoc.Nums); idx2++ {
			for idx3 := idx2 + 1; idx3 < len(aoc.Nums); idx3++ {
				if n+aoc.Nums[idx2]+aoc.Nums[idx3] == 2020 {
					fmt.Println(n * aoc.Nums[idx2] * aoc.Nums[idx3])
				}
			}
		}
	}
	fmt.Println("brute 3 Took", time.Since(start))
}
