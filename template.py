#!/usr/bin/env python2
from aoc import *

if test_mode:
    data = """
""".strip()

lines = list(each_line(data))

for l in lines:
    idx, groups = match(l, "re1", "re2", '(.*)')
