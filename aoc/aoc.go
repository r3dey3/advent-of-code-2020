package aoc

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
)

var Contents []byte
var FileRead bool
var Lines []string
var Nums []int

func init() {
	var err error
	var path string
	test_mode := flag.Bool("t", false, "Use the test input")
	test_mode2 := flag.Bool("t2", false, "Use the test input 2")
	flag.Parse()
	if *test_mode2 {
		path = fmt.Sprintf("%s_input_test2.txt", filepath.Base(os.Args[0]))
	} else if *test_mode {
		path = fmt.Sprintf("%s_input_test.txt", filepath.Base(os.Args[0]))
	} else {
		path = fmt.Sprintf("%s_input.txt", filepath.Base(os.Args[0]))
	}
	Contents, err = ioutil.ReadFile(path)
	if err != nil {
		fmt.Println("File reading error", err)
		return
	}

	file, err := os.Open(path)
	if err != nil {
		return
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		Lines = append(Lines, scanner.Text())
	}

	for _, l := range Lines {
		num, err := strconv.Atoi(l)
		if err != nil {
			break
		}

		Nums = append(Nums, num)
	}

	FileRead = true
}
func Min(x, y int) int {
	if x > y {
		return y
	}
	return x
}

func Max(x, y int) int {
	if x < y {
		return y
	}
	return x
}
