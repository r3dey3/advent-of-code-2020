// hello.go

package main

import (
	//"flag"
	"fmt"
	//"io/ioutil"
	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
	//"sort"
	//"strconv"
	//"strings"
	//"time"
)

func main() {

	//aoc.Lines = []string{"FBFBBFFRLR", "BFFFBBFRRR"}

	//all := make(map[rune][int])
	count := 0
	all_count := 0
	ans := make(map[rune]int)
	group_num := 0
	for _, l := range aoc.Lines {
		if l == "" {
			count += len(ans)
			for _, v := range ans {
				if v == group_num {
					all_count += 1
				}
			}
			ans = make(map[rune]int)
			group_num = 0
			continue
		}
		group_num += 1
		for _, c := range l {
			ans[c] += 1
		}

	}
	count += len(ans)
	for _, v := range ans {
		if v == group_num {
			all_count += 1
		}
	}
	fmt.Println(count)
	fmt.Println(all_count)
}
