// hello.go

package main

import (
	"fmt"

	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
)

/*
func dup(board [][]int) [][]int {
	ret := make([][]int, len(board))
	for i, v := range board {
		ret[i] = make([]int, len(v))
		copy(ret[i], v)
	}
	return ret
}
*/
func dup(board []string) []string {
	ret := make([]string, len(board))
	copy(ret, board)
	return ret
}
func count(x, y int, board []string) int {
	cnt := 0
	for i := y - 1; i <= y+1; i++ {
		if i < 0 {
			continue
		}
		if i >= len(board) {
			continue
		}
		for j := x - 1; j <= x+1; j++ {
			if j < 0 {
				continue
			}
			if j >= len(board[i]) {
				continue
			}
			if i == y && j == x {
				continue
			}
			if board[i][j] == '#' {
				cnt++
			}
		}
	}
	return cnt
}

type Seat struct {
	x, y int
}

func find_neighbor(start_x, start_y int, dx, dy int, board []string) (bool, Seat) {
	var ret Seat
	y := start_y + dy
	x := start_x + dx
	for ; y >= 0 && y < len(board) && x >= 0 && x < len(board[y]); x, y = x+dx, y+dy {
		if board[y][x] != '.' {
			return true, Seat{x, y}
		}
	}
	return false, ret
}

func find_neighbors(x, y int, board []string) []Seat {
	ret := make([]Seat, 0)

	for dy := -1; dy <= 1; dy++ {
		for dx := -1; dx <= 1; dx++ {
			if dx == 0 && dy == 0 {
				continue
			}
			found, s := find_neighbor(x, y, dx, dy, board)
			if found {
				ret = append(ret, s)
				//fmt.Println(x, y, s)
			}
		}
	}
	//fmt.Println(x, y, ret)
	return ret
}

func count2(x, y int, board []string, neighbors [][][]Seat) int {
	cnt := 0
	for _, n := range neighbors[y][x] {
		if board[n.y][n.x] == '#' {
			cnt += 1
		}
	}
	return cnt
}

func main() {
	board := dup(aoc.Lines)

	changed := true
	for changed {
		/*
				for _, r := range board {
					fmt.Println(r)
				}
			fmt.Println("")
		*/
		changed = false
		next := make([]string, len(board))

		for y := 0; y < len(board); y++ {
			row := ""
			for x := 0; x < len(board[y]); x++ {
				cur := board[y][x]
				if cur != '.' {
					cnt := count(x, y, board)
					if cur == '#' && cnt >= 4 {
						cur = 'L'
						changed = true
					} else if cur == 'L' && cnt == 0 {
						cur = '#'
						changed = true
					}
				}
				row += string(cur)
			}

			next[y] = row
		}
		board = next
	}
	cnt := 0
	for y := 0; y < len(board); y++ {
		for x := 0; x < len(board[y]); x++ {
			if board[y][x] == '#' {
				cnt++
			}
		}
	}
	fmt.Println(cnt)

	board = dup(aoc.Lines)
	neighbors := make([][][]Seat, len(board))

	for y := 0; y < len(board); y++ {
		neighbors[y] = make([][]Seat, len(board[y]))
		for x := 0; x < len(board[y]); x++ {
			if board[y][x] != '.' {
				neighbors[y][x] = find_neighbors(x, y, board)
			}
		}
	}

	//fmt.Println(neighbors)
	changed = true
	for changed {
		/*
			for _, r := range board {
				fmt.Println(r)
			}
			fmt.Println("")
		*/
		changed = false
		next := make([]string, len(board))

		for y := 0; y < len(board); y++ {
			row := ""
			for x := 0; x < len(board[y]); x++ {
				cur := board[y][x]
				if cur != '.' {
					cnt := count2(x, y, board, neighbors)
					if cur == '#' && cnt >= 5 {
						cur = 'L'
						changed = true
					} else if cur == 'L' && cnt == 0 {
						cur = '#'
						changed = true
					}
				}
				row += string(cur)
			}

			next[y] = row
		}
		board = next
	}
	cnt = 0
	for y := 0; y < len(board); y++ {
		for x := 0; x < len(board[y]); x++ {
			if board[y][x] == '#' {
				cnt++
			}
		}
	}
	fmt.Println(cnt)
}
