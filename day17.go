// hello.go

package main

import (
	"fmt"

	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
)

func set(x, y, z, w int, field map[Coord]bool) {
	field[Coord{x, y, z, w}] = true
}
func check(x, y, z, w int, field map[Coord]bool) bool {
	_, ok := field[Coord{x, y, z, w}]
	return ok
}

func count_near(x_in, y_in, z_in, w_in int, field map[Coord]bool) int {
	count := 0
	for x := -1; x <= 1; x++ {
		for y := -1; y <= 1; y++ {
			for z := -1; z <= 1; z++ {
				for w := -1; w <= 1; w++ {
					if w == 0 && x == 0 && y == 0 && z == 0 {
						continue
					}
					if check(x_in+x, y_in+y, z_in+z, w_in+w, field) {
						count++
					}
				}
			}
		}
	}
	return count
}

type Coord struct {
	x, y, z, w int
}

func main() {
	fmt.Println(len(aoc.Lines))
	field := make(map[Coord]bool, 0)

	for y, v_y := range aoc.Lines {
		for x, v_x := range v_y {
			if v_x == '#' {
				field[Coord{x, y, 0, 0}] = true
			}
		}
	}

	min_w := 0
	max_w := 0
	min_z := 0
	max_z := 0
	min_x := 0
	max_x := len(aoc.Lines) - 1
	min_y := 0
	max_y := len(aoc.Lines[0]) - 1

	fmt.Println(field)
	for w := min_w; w <= max_w; w++ {
		fmt.Println("W = ", w)
		for z := min_z; z <= max_z; z++ {
			fmt.Println("Z = ", z)
			for y := min_y; y <= max_y; y++ {
				for x := min_x; x <= max_x; x++ {
					if check(x, y, z, w, field) {
						fmt.Printf("#")
					} else {
						fmt.Printf(".")
					}
				}
				fmt.Println("")
			}
			fmt.Println("")
		}
	}

	for i := 0; i < 6; i++ {
		min_w--
		max_w++
		min_z--
		max_z++
		min_x--
		max_x++
		min_y--
		max_y++
		new_f := make(map[Coord]bool, 0)
		for w := min_w; w <= max_w; w++ {
			for z := min_z; z <= max_z; z++ {
				for y := min_y; y <= max_y; y++ {
					for x := min_x; x <= max_x; x++ {
						near := count_near(x, y, z, w, field)
						if check(x, y, z, w, field) {
							if near == 2 || near == 3 {
								set(x, y, z, w, new_f)
							}
						} else {
							if near == 3 {
								set(x, y, z, w, new_f)
							}
						}
					}
				}
			}
		}
		field = new_f
	}

	fmt.Println(len(field))

}
