#!/usr/bin/env python2
from aoc import *

if test_mode:
    data = """
""".strip()

lines = list(each_line(data))
valid = 0
valid2 = 0
for l in lines:
    idx, groups = match(l, "(\d+)-(\d+) (.): (.*)")
    c = 0
    for l in groups[3]:
        if l == groups[2]:
            c += 1
    if c >= groups[0] and c <= groups[1]:
        valid += 1
    try:
        c1 =groups[3][groups[0]-1]
    except IndexError:
        c1 = ''
    try:
        c2 =groups[3][groups[1]-1]
    except IndexError:
        c2 = ''

    if (c1 == groups[2] or c2 == groups[2]) and c1 != c2:
         valid2 += 1
print valid
print valid2


