// hello.go

package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
)

type Op int

const (
	Mult Op = iota
	Add
	Paren
)

type Level struct {
	op  Op
	val int
}

func (o Op) String() string {
	return [...]string{"*", "+", "("}[o]
}

func main() {
	sum := 0
	for _, l := range aoc.Lines {
		level := 0
		levels := make([]Level, 15)
		levels[0] = Level{Add, 0}
	Eval:
		for _, s := range strings.Split(l, " ") {
			if s == "+" {
				levels[level].op = Add
				continue Eval
			} else if s == "*" {
				levels[level].op = Mult
				continue Eval
			}
			num_open := 0
			num_close := 0
			for s[num_open] == '(' {
				num_open++
			}
			for s[len(s)-num_close-1] == ')' {
				num_close++
			}
			val, _ := strconv.Atoi(s[num_open : len(s)-num_close])

			for i := 0; i < num_open; i++ {
				level++
				levels[level] = Level{Add, 0}
			}

			switch levels[level].op {
			case Add:
				levels[level].val += val
			case Mult:
				levels[level].val *= val
			}

			for i := 0; i < num_close; i++ {
				val = levels[level].val
				level--
				switch levels[level].op {
				case Add:
					levels[level].val += val
				case Mult:
					levels[level].val *= val
				}
			}
		}
		fmt.Println(levels[0].val)
		sum += levels[0].val
	}
	fmt.Println(sum)
	fmt.Println("PART2")

	sum = 0
	for _, l := range aoc.Lines {
		op_stack := make([]Op, 50)
		val_stack := make([]int, 50)
		val_idx := -1
		op_idx := -1

	Eval2:
		for _, s := range strings.Split(l, " ") {
			/*
				if op_idx >= 0 {
					fmt.Print("OP", op_stack[0:op_idx+1])
				}
				if val_idx >= 0 {
					fmt.Print("  VAL", val_stack[0:val_idx+1])
				}
				fmt.Println("    ", s)
			*/
			if s == "+" {
				op_idx++
				op_stack[op_idx] = Add
				continue Eval2
			} else if s == "*" {
				if op_idx == -1 || op_stack[op_idx] != Add {
					op_idx++
					op_stack[op_idx] = Mult
					continue Eval2
				}
				for op_idx >= 0 && op_stack[op_idx] != Paren {
					val1 := val_stack[val_idx]
					val_idx--
					val2 := val_stack[val_idx]
					switch op_stack[op_idx] {
					case Add:
						val_stack[val_idx] = val1 + val2
					case Mult:
						val_stack[val_idx] = val1 * val2
					}
					op_idx--
				}
				op_idx++
				op_stack[op_idx] = Mult

				continue Eval2
			}

			num_open := 0
			num_close := 0
			for s[num_open] == '(' {
				num_open++
				op_idx++
				op_stack[op_idx] = Paren

			}

			for s[len(s)-num_close-1] == ')' {
				num_close++
			}

			val, _ := strconv.Atoi(s[num_open : len(s)-num_close])

			val_idx++
			val_stack[val_idx] = val

			for i := 0; i < num_close; i++ {
				for op_idx >= 0 && op_stack[op_idx] != Paren {
					val1 := val_stack[val_idx]
					val_idx--
					val2 := val_stack[val_idx]
					switch op_stack[op_idx] {
					case Add:
						val_stack[val_idx] = val1 + val2
					case Mult:
						val_stack[val_idx] = val1 * val2
					}
					op_idx--
				}
				op_idx--
			}
		}
		/*
			if op_idx >= 0 {
				fmt.Print("OP", op_stack[0:op_idx+1])
			}
			if val_idx >= 0 {
				fmt.Println("  VAL", val_stack[0:val_idx+1])
			}
		*/
		for op_idx >= 0 {
			val1 := val_stack[val_idx]
			val_idx--
			val2 := val_stack[val_idx]
			//fmt.Println(op_stack[op_idx], val1, val2)
			switch op_stack[op_idx] {
			case Add:
				val_stack[val_idx] = val1 + val2
			case Mult:
				val_stack[val_idx] = val1 * val2
			}
			op_idx--
		}
		fmt.Println(val_stack[0])
		sum += val_stack[0]
	}
	fmt.Println(sum)
}
