// hello.go

package main

import (
	"fmt"
	"strconv"

	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
)

func move(x, y, v int, dir rune) (int, int) {
	pos_x := x
	pos_y := y
	switch dir {
	case 'N':
		pos_y += v
	case 'S':
		pos_y -= v
	case 'E':
		pos_x += v
	case 'W':
		pos_x -= v
	}
	return pos_x, pos_y
}

func main() {
	fmt.Println(len(aoc.Lines))
	pos_x, pos_y := 0, 0
	dir := 'E'
	rot_R := map[rune]rune{
		'E': 'S',
		'S': 'W',
		'W': 'N',
		'N': 'E',
	}
	rot_L := map[rune]rune{
		'E': 'N',
		'N': 'W',
		'W': 'S',
		'S': 'E',
	}
	for _, l := range aoc.Lines {
		v, _ := strconv.Atoi(l[1:])
		switch l[0] {
		case 'N':
			fallthrough
		case 'S':
			fallthrough
		case 'E':
			fallthrough
		case 'W':
			pos_x, pos_y = move(pos_x, pos_y, v, rune(l[0]))
		case 'F':
			pos_x, pos_y = move(pos_x, pos_y, v, dir)
		case 'R':
			for ; v > 0; v -= 90 {
				dir = rot_R[dir]
			}
		case 'L':
			for ; v > 0; v -= 90 {
				dir = rot_L[dir]
			}
		}
	}
	fmt.Println(pos_x, pos_y)

	wp_x, wp_y := 10, 1
	x, y := 0, 0

	for _, l := range aoc.Lines {
		v, _ := strconv.Atoi(l[1:])
		switch l[0] {
		case 'N':
			wp_y += v
		case 'S':
			wp_y -= v
		case 'E':
			wp_x += v
		case 'W':
			wp_x -= v
		case 'F':
			x += wp_x * v
			y += wp_y * v
		case 'L':
			v = 360 - v
			fallthrough
		case 'R':
			for ; v > 0; v -= 90 {
				orig_wp_x := wp_x
				wp_x = wp_y
				wp_y = -1 * orig_wp_x

			}

		}
	}
	fmt.Println(x, y)
	fmt.Println(x + y)

	/*

		N 1, E 5  -- 5, 1
		E 1, S 5  -- 1, -5
		W 5, S 1  -- -5, -1
		N 5, W 1 -- -1, 5
	*/
}
