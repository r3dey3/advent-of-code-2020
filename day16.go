// hello.go

package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
)

type Field struct {
	name        string
	low1, high1 int
	low2, high2 int
	index       int
}

func main() {
	state := "fields"
	fields := make([]Field, 0)
	error_rate := 0
	my_ticket := make([]int, 0)
	tickets := make([][]int, 0)
	for _, l := range aoc.Lines {
		if l == "" {
			continue
		}
		if state == "fields" {
			if l == "your ticket:" {
				state = "mine"
				continue
			}
			var f Field
			var n2 string
			r, _ := fmt.Sscanf(l, "%s %s %d-%d or %d-%d", &f.name, &n2, &f.low1, &f.high1, &f.low2, &f.high2)
			if r == 6 {
				f.name += " " + n2
			} else {
				fmt.Sscanf(l, "%s %d-%d or %d-%d", &f.name, &f.low1, &f.high1, &f.low2, &f.high2)
			}
			f.index = -1
			f.name = f.name[:len(f.name)-1]
			fmt.Println(f)
			fields = append(fields, f)
		} else if state == "mine" {
			if l == "nearby tickets:" {
				state = "others"
				continue
			}

			for _, s := range strings.Split(l, ",") {
				n, _ := strconv.Atoi(s)
				my_ticket = append(my_ticket, n)
			}
		} else if state == "others" {
			ticket := make([]int, 0)
			valid := true
			for _, s := range strings.Split(l, ",") {
				found := false
				n, _ := strconv.Atoi(s)
				for _, f := range fields {
					if (n >= f.low1 && n <= f.high1) || (n >= f.low2 && n <= f.high2) {
						found = true
						break
					}
				}
				ticket = append(ticket, n)
				if !found {
					valid = false
					error_rate += n
				}
			}
			if valid {
				tickets = append(tickets, ticket)
			}
		}
	}
	fmt.Println(error_rate)
	//fmt.Println(tickets)
	num_left := len(fields)
	used := make([]bool, len(fields))
	for num_left > 0 {
		for f_idx, f := range fields {
			if f.index != -1 {
				continue
			}
			match := make([]int, 0)
			for i, u := range used {
				if u {
					continue
				}
				match_all := true
				for _, t := range tickets {
					if !((t[i] >= f.low1 && t[i] <= f.high1) || (t[i] >= f.low2 && t[i] <= f.high2)) {
						match_all = false
						break
					}
				}
				if match_all {
					match = append(match, i)
				}
			}
			//fmt.Println(f.name, len(match))
			if len(match) == 1 {
				idx := match[0]
				fields[f_idx].index = idx
				used[idx] = true
				num_left--
			}
		}
	}
	m := 1
	for _, f := range fields {
		if strings.HasPrefix(f.name, "departure") {
			fmt.Println(f)
			m *= my_ticket[f.index]
		}
	}

	fmt.Println(m)
}
