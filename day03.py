#!/usr/bin/env python2
from aoc import *

if test_mode:
    data = """
""".strip()

lines = list(each_line(data))

row = 0
col = 0
trees = 0
while row < len(lines):
    if lines[row][col] == '#':
        trees += 1
    col = (col + 3) % len(lines[row])
    row += 1

print trees

slopes = [(1,1), (3, 1), (5, 1), (7, 1), (1, 2)]

mult = 1
for right, down in slopes:
    row = 0
    col = 0
    trees = 0
    while row < len(lines):
        if lines[row][col] == '#':
            trees += 1
        col = (col + right) % len(lines[row])
        row += down
    mult *= trees
print mult

