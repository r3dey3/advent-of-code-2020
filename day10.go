// hello.go

package main

import (
	"fmt"
	"sort"
	"time"

	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
)

func count(start_idx int) int {
	cur := 0
	if start_idx >= 0 {
		if counts[start_idx] != 0 {
			return counts[start_idx]
		}
		cur = aoc.Nums[start_idx]
		if cur == device {
			return 1
		}
	}

	//fmt.Println(start_idx)
	cnt := 0
	for i := 1; i <= 3; i++ {
		if start_idx+i >= len(aoc.Nums) {
			break
		}
		n := aoc.Nums[start_idx+i]
		//fmt.Println(cur, n)
		if n-cur <= 3 {
			cnt += count(start_idx + i)
		}
	}
	if start_idx >= 0 {
		counts[start_idx] = cnt
	}
	return cnt
}

var device int
var counts []int

func main() {
	sort.Ints(aoc.Nums)

	prev := 0
	num_1 := 0
	num_3 := 1
	for _, v := range aoc.Nums {
		switch v - prev {
		case 1:
			num_1++
		case 2:
		case 3:
			num_3++
		default:
			fmt.Println("ERROR")
		}
		prev = v
		device = v
	}
	counts = make([]int, len(aoc.Nums))
	fmt.Println(num_1 * num_3)

	start := time.Now()
	fmt.Println(count(-1))
	fmt.Println("Took", time.Since(start))
	//fmt.Println(aoc.Nums)
	//fmt.Println(counts)

}
