// hello.go

package main

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
)

type Bus struct {
	id int
	at int
}

func main() {
	bus := aoc.Nums[0]
	earliest := 0xffffffffff
	early_id := 0
	buses := make([]Bus, 0)
	for i, s := range strings.Split(aoc.Lines[1], ",") {
		if s == "x" {
			continue
		}
		n, _ := strconv.Atoi(s)

		buses = append(buses, Bus{n, i})
		t := (bus / n) * n
		if bus%n != 0 {
			t += n
		}
		if t < earliest {
			early_id = n
			earliest = t
		}
	}
	fmt.Println(early_id * (earliest - bus))
	fmt.Println(buses)

	for i, b := range buses {
		if i != 0 {
			buses[i] = Bus{b.id, b.at % b.id}
		}
	}
	fmt.Println(buses)

	step := buses[0].id
	c_buses := make([]Bus, 0)
	for _, b := range buses[1:] {
		if b.at == buses[0].id {
			step *= b.id
		} else {
			c_buses = append(c_buses, b)
		}
	}
	fmt.Println(c_buses)
	fmt.Println(step)
	t := step - buses[0].id
	start := time.Now()
	for {
		valid := true
		for _, b := range c_buses {
			if (t+b.at)%b.id != 0 {
				valid = false
				break
			}
		}
		if valid {
			fmt.Println(t)
			break
		}
		t += step
	}
	fmt.Println("took", time.Since(start))
}
