import argparse
import md5
import os
import Queue
import re
import subprocess
import sys

#__all__ = ["data", "each_line", 'test_mode', 'match', 'match_list']

try:
    day = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    m = re.match('(day\d\d)', day)
    day = m.group(1)
    with open("%s_input.txt" % day) as f:
        data = f.read()
except:
    data = ""

def each_line(data):
    for l in data.split("\n"):
        if l:
            yield l

def try_int(value):
    try:
        return int(value, 0)
    except:
        return value

def match(line, *relist, **kwargs):
    try_cast = kwargs.pop('try_cast', try_int)
    for i,r in enumerate(relist):
        kwargs[i] = r
    for k, r in kwargs.items():
        if isinstance(r, (str, unicode)):
            m = re.match(r, line)
        else:
            m = r.match(line)
        if m:
            return k, map(try_cast, m.groups())

    sys.stderr.write('Failed to match line %r\n' % (line))
    sys.exit(1)

parser = argparse.ArgumentParser()
parser.add_argument('-t','--test', dest='test_mode', action='store_const',
        const=True, default=False)

args = parser.parse_args()
test_mode = args.test_mode
