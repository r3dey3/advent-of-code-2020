// hello.go

package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
)

func run(code []string, chg int) (bool, int) {
	pc := 0
	acc := 0
	exec := make([]bool, len(code))
	for pc < len(code) && pc >= 0 {
		if exec[pc] {
			return false, acc
		}
		exec[pc] = true
		words := strings.Split(code[pc], " ")
		//fmt.Println(pc, words)
		val, _ := strconv.Atoi(words[1])
		if pc == chg {
			switch words[0] {
			case "acc":
				acc += val
			case "nop":
				pc += val - 1
			}
		} else {
			switch words[0] {
			case "acc":
				acc += val
			case "jmp":
				pc += val - 1
			}
		}
		pc += 1
	}
	return pc > 0, acc
}
func main() {

	_, acc := run(aoc.Lines, -1)
	fmt.Println(acc)

	for i := range aoc.Lines {
		valid, acc := run(aoc.Lines, i)
		if valid {
			fmt.Println(i, acc)
		}
	}
	fmt.Println("")

}
