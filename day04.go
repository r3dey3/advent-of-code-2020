// hello.go

package main

import (
	//"flag"
	"fmt"
	//"io/ioutil"
	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
	"strconv"
	"strings"
	"time"
)

/*
   byr (Birth Year)
   iyr (Issue Year)
   eyr (Expiration Year)
   hgt (Height)
   hcl (Hair Color)
   ecl (Eye Color)
   pid (Passport ID)
   cid (Country ID)
*/

//struct {
//byr, iyr, eyr, hgt, hcl, ecl, pid, cid string
//}

func valid(p map[string]string) bool {

	num, err := strconv.Atoi(p["byr"])
	if err != nil {
		return false
	}
	if num < 1920 || num > 2002 {
		return false
	}

	num, err = strconv.Atoi(p["iyr"])
	if err != nil {
		return false
	}
	if num < 2010 || num > 2020 {
		return false
	}

	num, err = strconv.Atoi(p["eyr"])
	if err != nil {
		return false
	}
	if num < 2020 || num > 2030 {
		return false
	}

	if strings.HasSuffix(p["hgt"], "cm") {
		num, err = strconv.Atoi(p["hgt"][0 : len(p["hgt"])-2])
		if err != nil {
			return false
		}
		if num < 150 || num > 193 {
			return false
		}
	} else if strings.HasSuffix(p["hgt"], "in") {
		num, err = strconv.Atoi(p["hgt"][0 : len(p["hgt"])-2])
		if err != nil {
			return false
		}
		if num < 59 || num > 76 {
			return false
		}

	} else {
		return false
	}

	ecl := p["ecl"]
	if ecl != "amb" && ecl != "blu" && ecl != "brn" && ecl != "gry" && ecl != "grn" && ecl != "hzl" && ecl != "oth" {
		return false
	}

	if len(p["hcl"]) != 7 {
		return false
	}
	if p["hcl"][0:1] != "#" {
		return false
	}
	_, err = strconv.ParseInt(p["hcl"][1:7], 16, 32)
	if err != nil {
		return false
	}

	if len(p["pid"]) != 9 {
		return false
	}
	num, err = strconv.Atoi(p["pid"])
	if err != nil {
		return false
	}
	return true
}
func main() {
	count := 0
	count2 := 0
	//var p map[string]string
	p := make(map[string]string)
	start := time.Now()
	for _, l := range aoc.Lines {
		if l == "" {
			if p["byr"] != "" &&
				p["iyr"] != "" &&
				p["eyr"] != "" &&
				p["hgt"] != "" &&
				p["hcl"] != "" &&
				p["ecl"] != "" &&
				p["pid"] != "" {

				count++
				if valid(p) {
					count2++
				}
			}
			p = make(map[string]string)
		} else {
			for _, w := range strings.Split(l, " ") {
				f := strings.Split(w, ":")
				p[f[0]] = f[1]
			}
		}
	}
	fmt.Println("Took", time.Since(start))
	fmt.Println(count)
	fmt.Println(count2)
}
