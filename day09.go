// hello.go

package main

import (
	"fmt"

	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
)

const NUM_SUMS = 25

func Min(x, y int) int {
	if x > y {
		return y
	}
	return x
}

func Max(x, y int) int {
	if x < y {
		return y
	}
	return x
}
func main() {
	sums := make([][]int, NUM_SUMS)

	for i := 0; i < NUM_SUMS; i++ {
		sums[i] = make([]int, 0, NUM_SUMS)
		for j := i + 1; j < NUM_SUMS; j++ {
			sums[i] = append(sums[i], aoc.Nums[i]+aoc.Nums[j])
		}
	}
	//fmt.Println(sums)
	bad_num := 0
	for i, v := range aoc.Nums {
		if i < NUM_SUMS {
			continue
		}
		found := false
		for _, s := range sums {
			for _, sv := range s {
				if sv == v {
					found = true
				}
			}
		}
		if !found {
			bad_num = v
			fmt.Println(v)
			break
		}

		sums = sums[1:NUM_SUMS]
		for j := range sums {
			sums[j] = append(sums[j], v+aoc.Nums[i-NUM_SUMS+j+1])
		}

		sums = append(sums, make([]int, 0, NUM_SUMS))
		//fmt.Println(sums)
	}

Loop:
	for i, v := range aoc.Nums {
		sum := v
		min := v
		max := v
		for j := i + 1; j < len(aoc.Nums); j++ {
			v2 := aoc.Nums[j]
			min = Min(min, v2)
			max = Max(max, v2)
			sum += v2
			if sum == bad_num {
				fmt.Println(min + max)
				break Loop
			}
			if sum > bad_num {
				break
			}
		}

	}

	fmt.Println("")

}
