// hello.go

package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
)

func main() {
	spoken := make(map[int]int)

	idx := 1
	last := -1

	for _, w := range strings.Split(aoc.Lines[0], ",") {
		n, _ := strconv.Atoi(w)
		fmt.Println(idx, n)

		last = n
		spoken[last] = idx
		idx++
	}
	//fmt.Println(spoken)

	for ; idx <= 30000000; idx++ {
		//fmt.Println(spoken)
		l_idx, ok := spoken[last]
		spoken[last] = idx - 1
		num := 0
		if ok {
			//fmt.Println("--", l_idx, idx)
			num = idx - l_idx - 1
		}
		//fmt.Println(idx, num)
		last = num

	}
	//	fmt.Println(spoken)
	fmt.Println(last)
}
