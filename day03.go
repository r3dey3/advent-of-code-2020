// hello.go

package main

import (
	//"flag"
	"fmt"
	//"io/ioutil"
	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
)

func calc(right int, down int) int {
	row := 0
	col := 0
	trees := 0
	for row < len(aoc.Lines) {
		if aoc.Lines[row][col] == '#' {
			trees += 1
		}
		col = (col + right) % len(aoc.Lines[0])
		row += down
	}
	return trees
}
func main() {
	if aoc.FileRead {
		fmt.Println("Successfully Read File")
	}
	fmt.Println("Trees = ", calc(3, 1))
	fmt.Println("Mult = ", calc(1, 1)*calc(3, 1)*calc(5, 1)*calc(7, 1)*calc(1, 2))
}
