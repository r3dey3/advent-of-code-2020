#!/usr/bin/env python2
from aoc import *
import itertools
import time

if test_mode:
    data = """
""".strip()

lines = list( int(i) for i in each_line(data))


start = time.time()
for x,y in itertools.combinations(lines, 2):
    if x+y == 2020:
        print x*y
print "Took", time.time()-start
start = time.time()

for x,y,z in itertools.combinations(lines, 3):
    if x+y+z == 2020:
        print x*y*z
print "Took", time.time()-start
start = time.time()

for idx, v in enumerate(lines):
    idx2 = idx + 1
    while idx2 < len(lines):
        if v + lines[idx2] == 2020:
            print v *lines[idx2]
        idx2 += 1
    
print "Took", time.time()-start
start = time.time()
for idx, v in enumerate(lines):
    idx2 = idx + 1
    while idx2 < len(lines):
        idx3 = idx2 + 1
        while idx3 < len(lines):
            if v + lines[idx2] + lines[idx3]== 2020:
                print v *lines[idx2]*lines[idx3]
            idx3 += 1
        idx2 += 1
print "Took", time.time()-start
