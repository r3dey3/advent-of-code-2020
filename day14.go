// hello.go

package main

import (
	"fmt"
	"log"
	"regexp"
	"strconv"

	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
)

var wild_mem map[int]int

func write(addr, val int, cur_wild, wild_mask int) {
	//fmt.Println(cur_wild)
	for ; cur_wild < 36; cur_wild++ {
		if wild_mask&(1<<cur_wild) != 0 {
			write(addr, val, cur_wild+1, wild_mask)
			write(addr|(1<<cur_wild), val, cur_wild+1, wild_mask)
			return
		}
	}
	//fmt.Printf("%036b %d = %d\n", addr, addr, val)
	wild_mem[addr] = val
}
func main() {
	mem := make(map[int]int)
	wild_mem = make(map[int]int)

	mask_re, err := regexp.Compile("^mask = ([01X]+)$")
	if err != nil {
		log.Fatal(err)
	}

	mem_re, err := regexp.Compile("^mem\\[(\\d+)\\] = (\\d+)$")
	if err != nil {
		log.Fatal(err)
	}

	mask_or := 0
	mask_and := 0
	mask_and_wild := 0
	mask_wild := 0

	for _, l := range aoc.Lines {
		m := mask_re.FindStringSubmatch(l)
		if m != nil {
			mask_or = 0
			mask_and = 0
			mask_and_wild = 0
			mask_wild = 0
			for _, c := range m[1] {
				mask_or <<= 1
				mask_and <<= 1
				mask_and_wild <<= 1
				mask_wild <<= 1
				switch c {
				case 'X':
					mask_and |= 1
					mask_wild |= 1
					mask_and_wild |= 0
				case '0':
					mask_and |= 0
					mask_and_wild |= 1
				case '1':
					mask_and_wild |= 1
					//mask_and |= 1
					mask_or |= 1
				}
			}
			/*
				fmt.Printf("         %s\n", m[1])
				fmt.Printf("mask_and %036b\nmask_or  %036b\n", mask_and, mask_or)
				fmt.Printf("and_wild %036b\nmask_wid %036b\n", mask_and_wild, mask_wild)
			*/
		}
		m = mem_re.FindStringSubmatch(l)
		if m != nil {
			addr, _ := strconv.Atoi(m[1])
			val, _ := strconv.Atoi(m[2])
			write((addr&mask_and_wild)|mask_or, val, 0, mask_wild)
			val &= mask_and
			val |= mask_or
			//fmt.Println(val)
			mem[addr] = val
			continue
		}
	}
	sum := 0
	for _, v := range mem {
		sum += v
	}
	fmt.Println(sum)

	sum = 0
	for _, v := range wild_mem {
		sum += v
	}
	fmt.Println(sum)
}
