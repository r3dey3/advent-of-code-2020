// hello.go

package main

import (
	//"flag"
	"fmt"
	//"io/ioutil"
	"gitlab.com/r3dey3/advent-of-code-2020/aoc"
	"sort"
	"strconv"
	"strings"
	//"time"
)

var bags map[string][]string

type bag struct {
	name  string
	count int
}

func compute(bag string) []string {
	f, ok := found_bags[bag]
	if ok {
		return f
	}

	ret := make([]string, 0)
	for _, b := range all_bags[bag] {
		ret = append(ret, b.name)
	}

	for _, v := range all_bags[bag] {
		s := compute(v.name)
		for _, n_v := range s {
			found := false
			for _, cur := range ret {
				if n_v == cur {
					found = true
				}
			}
			if !found {
				ret = append(ret, n_v)
			}
		}
	}
	sort.Strings(ret)
	found_bags[bag] = ret
	return ret
}

func count_bags(name string) int {
	cnt := 1
	for _, v := range all_bags[name] {
		cnt += v.count * count_bags(v.name)
	}
	fmt.Println(name, cnt)
	return cnt
}

var all_bags map[string][]bag
var found_bags map[string][]string

func main() {
	all_bags = make(map[string][]bag, 0)
	found_bags = make(map[string][]string, 0)

	for _, l := range aoc.Lines {
		words := strings.Split(l, " ")
		bag_name := words[0] + " " + words[1]
		/*b, ok := bags[bag]
		if !ok {
			bags[bag] = make([]string)

		}*/
		this_bag := make([]bag, 0)
		if words[4] != "no" {
			for idx := 4; idx < len(words); idx += 4 {
				c_bag_name := words[idx+1] + " " + words[idx+2]
				count, _ := strconv.Atoi(words[idx])
				c_bag := bag{c_bag_name, count}
				this_bag = append(this_bag, c_bag)
			}
		}

		all_bags[bag_name] = this_bag
	}
	//fmt.Println(all_bags)

	for k, _ := range all_bags {
		compute(k)
	}

	//fmt.Println(found_bags)
	count := 0
	for _, b := range found_bags {
		for _, bag := range b {
			if bag == "shiny gold" {
				count++
			}
		}
	}

	//#fmt.Println(k, found_bags[k])
	fmt.Println(count)
	fmt.Println(count_bags("shiny gold") - 1)
}
